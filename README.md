# hyperfOci8
hyperf连接oracle

原包为yajra/laravel-oci8 经修改后可连接oracle 试用版本

引入后需要修改连接工厂文件 增加oracle连接类型并引入相关库

vebdor/hyperf/database/src/Connectors/ConnectionFactory.php


```
\\引入依赖库
use Thinks\Oci8\Connectors\OracleConnector;
use Thinks\Oci8\Oci8Connection;

\\修改createConnector方法 增加如下代码
case 'oracle':
return new OracleConnector();


\\修改createConnection方法 增加如下代码
case 'oracle':
return new Oci8Connection($connection,$database,$prefix,$config);

```


